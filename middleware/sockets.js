const http = require("./http")
const io = require("socket.io")(http)
const logger = require("core/logger")

const socketEvents = require("constants/sockets")

// io.use((socket, next) => { sessionMiddleware(socket.request, {}, next) })

io.on("connection", (socket) => {
  socket.request.session.socketId = socket.id
  socket.request.session.save()
})

module.exports = io
