const mongoose = require("mongoose")
const { Category, SubCategory } = require("db/categories/collection")

describe("auth-methods", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Category.deleteMany({})
  })

  it("should create a subcategory", async () => {
    const data = {
      categoryId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      name: "Works",
    }

    await new SubCategory(data).save()
    const num = await SubCategory.countDocuments()
    expect(num).toBe(1)
  })
  it("should create a category", async () => {
    const data = {
      name: "Works",
    }

    await new Category(data).save()
    const num = await Category.countDocuments()
    expect(num).toBe(1)
  })

  it("should NOT let you insert an empty category", async () => {
    const fakeCategory = {}
    let error = null
    try {
      await new Category(fakeCategory).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })
})
