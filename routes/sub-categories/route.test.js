const mongoose = require("mongoose")
const { Category } = require("db/categories/collection")
const app = require("middleware/app")
const { responses } = require("db/categories/constants")
const supertest = require("supertest")

const categoryRouter = require("routes/categories/route")()
const subcategoryRouter = require("routes/sub-categories/route")()

const request = supertest(app)

describe("sub-categories route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Category.deleteMany({})
  })

  // CREATING SUBCATEGORIES
  it("creating a subcategory without a category Id should not work", async () => {
    app.use("/subcategories", subcategoryRouter)

    const res = await request.post("/subcategories")
      .send({
        name: "Test",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_CATEGORY_ID)
  })

  it("creating a subcategory with an invalid category Id should not work", async () => {
    app.use("/subcategories", subcategoryRouter)

    const res = await request.post("/subcategories")
      .send({
        name: "Test",
        categoryId: "fakefake",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.CATEGORY_ID_INVALID)
  })

  it("creating a subcategory with an inexisting category should not work", async () => {
    app.use("/subcategories", subcategoryRouter)

    const res = await request.post("/subcategories")
      .send({
        name: "Test",
        categoryId: "60895a1935f3351aabb10a2d",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_CATEGORY)
  })

  it("creating a subcategory without a name should not work", async () => {
    app.use("/subcategories", subcategoryRouter)

    const res = await request.post("/subcategories")
      .send({})
      .expect(400)

    expect(res.body.message).toBe(responses.NO_NAME)
  })

  it("creating a subcategory with a name that is already taken should not work", async () => {
    app.use("/", categoryRouter)
    app.use("/subcategories", subcategoryRouter)

    const catRes = await request.post("/create")
      .send({ name: "Test" })
      .expect(200)

    const res = await request.post("/subcategories")
      .send({
        name: "Test",
        categoryId: catRes.body.category._id,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NAME_TAKEN)
  })

  it("creating a subcategory with valid data should work", async () => {
    app.use("/", categoryRouter)
    app.use("/subcategories", subcategoryRouter)

    const catRes = await request.post("/create")
      .send({ name: "Category" })
      .expect(200)

    await request.post("/subcategories")
      .send({
        name: "SubCategory",
        categoryId: catRes.body.category._id,
      })
      .expect(200)
  })

  // FETCHING SUBCATEGORIES
  it("fetching subcategories for a valid category should work", async () => {
    app.use("/", categoryRouter)
    app.use("/subcategories", subcategoryRouter)

    const catRes = await request.post("/create")
      .send({ name: "Category" })
      .expect(200)

    const anotherCatRes = await request.post("/create")
      .send({ name: "Another Category" })
      .expect(200)

    await request.post("/subcategories")
      .send({
        name: "SubCategory 1",
        categoryId: catRes.body.category._id,
      })
      .expect(200)
    await request.post("/subcategories")
      .send({
        name: "SubCategory 2",
        categoryId: catRes.body.category._id,
      })
      .expect(200)

    let res = await request.get("/subcategories/by-category")
      .query({
        categoryId: catRes.body.category._id,
      })
      .expect(200)

    expect(res.body.total).toBe(2)

    res = await request.get("/subcategories/by-category")
      .query({
        categoryId: anotherCatRes.body.category._id,
      })
      .expect(200)
    expect(res.body.total).toBe(0)
  })

  it("fetching subcategories by category with invalid data should not work", async () => {
    app.use("/subcategories", subcategoryRouter)

    let res = await request.get("/subcategories/by-category")
      .expect(400)

    expect(res.body.message).toBe(responses.NO_CATEGORY_ID)

    res = await request.get("/subcategories/by-category")
      .query({
        categoryId: "fake",
      })
      .expect(400)

    res = await request.get("/subcategories/by-category")
      .query({
        categoryId: "60895a1935f3351aabb10a2d",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_CATEGORY)
  })

  it("fetching subcategories options for a valid category should work", async () => {
    app.use("/", categoryRouter)
    app.use("/subcategories", subcategoryRouter)

    const catRes = await request.post("/create")
      .send({ name: "Category" })
      .expect(200)

    const anotherCatRes = await request.post("/create")
      .send({ name: "Another Category" })
      .expect(200)

    await request.post("/subcategories")
      .send({
        name: "SubCategory 1",
        categoryId: catRes.body.category._id,
      })
      .expect(200)
    await request.post("/subcategories")
      .send({
        name: "SubCategory 2",
        categoryId: catRes.body.category._id,
      })
      .expect(200)

    let res = await request.get("/subcategories/by-category/options")
      .query({
        categoryId: catRes.body.category._id,
      })
      .expect(200)

    expect(res.body.options.length).toBe(2)

    res = await request.get("/subcategories/by-category/options")
      .query({
        categoryId: anotherCatRes.body.category._id,
      })
      .expect(200)
    expect(res.body.options.length).toBe(0)
  })
})
