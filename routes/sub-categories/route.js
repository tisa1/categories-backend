const mongoose = require("mongoose")
const express = require("express")
const errorService = require("lib/errors/handler")

// const jwtSecurity = require("core/jwt")
const { responses } = require("db/categories/constants")

// const _ = require("underscore")
const { SubCategory } = require("db/categories/collection")
const routeSecurity = require("./security")
const middleware = require("./middleware")

const createRouter = () => {
  const subCategoryRouter = express.Router()

  // TODO: secure route creation
  subCategoryRouter.post("/", [
    routeSecurity.create,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const data = req?.body
      const subCategory = await new SubCategory(data).save()

      return res.json({ message: responses.SUBCATEGORY_CREATED, subCategory })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on subcategories / create route",
      })
    }
  })

  subCategoryRouter.get("/by-category", [
    routeSecurity.byCategory,
  ], async (req, res, next) => {
    try {
      const { categoryId } = req.query

      const subCategories = await SubCategory.find({
        categoryId,
      })
      const total = await SubCategory.countDocuments({ categoryId })

      return res.json({ subCategories, total })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on subcategories / route",
      })
    }
  })

  subCategoryRouter.get("/by-category/options", [
    routeSecurity.byCategory,
  ], async (req, res, next) => {
    try {
      const { categoryId } = req.query

      const options = await SubCategory.find({
        categoryId,
      }, { name: 1 })

      return res.json({ options })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on subcategories /by-categories/options route",
      })
    }
  })

  // subCategoryRouter.delete("/single", [
  //   jwtSecurity.checkToken,
  //   routeSecurity.remove,
  // ], async (req, res, next) => {
  //   try {
  //     const subCategoryId = req?.body?.subCategoryId

  //     await Org.deleteOne({ _id: subCategoryId })
  //     await Category.deleteMany({ subCategoryId: subCategoryId })

  //     return res.json({ message: responses.ORGANISATION_DELETED })
  //   } catch (error) {
  //     return errorService.handle({
  //       req, res, next, message: "Error on categories /me route",
  //     })
  //   }
  // })

  return subCategoryRouter
}

module.exports = createRouter
