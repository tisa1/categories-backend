const mongoose = require("mongoose")
const { Category, SubCategory } = require("db/categories/collection")
const { responses } = require("db/categories/constants")

const create = async (req, res, next) => {
  const name = req?.body?.name
  if (!name) {
    return res.status(400).json({ message: responses.NO_NAME })
  }

  const existingCategory = await Category.findOne({ name })

  if (existingCategory) {
    return res.status(400).json({ message: responses.NAME_TAKEN })
  }

  return next()
}

const me = async (req, res, next) => {
// const userId = req?.decoded?.userId

  // if (!userId) {
  //   return res.status(400).json({ message: responses.NO_USER_ID })
  // }
  // if (!mongoose.Types.ObjectId.isValid(userId)) {
  //   return res.status(400).json({ message: responses.USER_ID_INVALID })
  // }
  // const existingCategory = await User.findById(userId, { _id: 1 })
  // if (!existingCategory) {
  //   return res.status(400).json({ message: responses.NO_USER_FOUND })
  // }

  next()
}

const remove = async (req, res, next) => {
  const categoryId = req?.body?.categoryId

  if (!categoryId) {
    return res.status(400).json({ message: responses.NO_CATEGORY_ID })
  }
  if (!mongoose.Types.ObjectId.isValid(categoryId)) {
    return res.status(400).json({ message: responses.CATEGORY_ID_INVALID })
  }
  const existingCategory = await Category.findById(categoryId, { _id: 1 })
  if (!existingCategory) {
    return res.status(400).json({ message: responses.NO_CATEGORY })
  }

  const hasSubcategories = await SubCategory.countDocuments({ categoryId })
  if (hasSubcategories) {
    return res.status(400).json({ message: responses.SUBCATEGORIES_LINKED })
  }

  next()
}

module.exports = {
  create,
  me,
  remove,
}
