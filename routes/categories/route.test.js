const mongoose = require("mongoose")
const { Category } = require("db/categories/collection")
const app = require("middleware/app")
const { responses } = require("db/categories/constants")
const supertest = require("supertest")

const categoryRouter = require("routes/categories/route")()
const subcategoryRouter = require("routes/sub-categories/route")()

const request = supertest(app)

const data = {
  name: "Shoes",
}

describe("categories route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Category.deleteMany({})
  })

  it("creating a category should work", async () => {
    app.use("/", categoryRouter)

    const res = await request.post("/create")
      .send(data)
      .expect(200)

    expect(res.body.message).toBe(responses.CATEGORY_CREATED)
  })

  // CREATING CATEGORIES
  it("creating a category with an already existing name should not work", async () => {
    app.use("/", categoryRouter)

    await request.post("/create")
      .send(data)
      .expect(200)

    const res = await request.post("/create")
      .send(data)
      .expect(400)

    expect(res.body.message).toBe(responses.NAME_TAKEN)
  })

  it("creating a category without a name should not work", async () => {
    app.use("/", categoryRouter)

    const res = await request.post("/create")
      .send({})
      .expect(400)

    expect(res.body.message).toBe(responses.NO_NAME)
  })

  // FETCHING CATEGORIES
  it("fetching categories should work", async () => {
    app.use("/", categoryRouter)

    await request.post("/create")
      .send({
        name: "Test",
      })
      .expect(200)
    await request.post("/create")
      .send({
        name: "Test 2",
      })
      .expect(200)

    const res = await request.get("/")
      .expect(200)

    expect(res.body.total).toBe(2)
  })

  it("fetching category graph should work", async () => {
    app.use("/", categoryRouter)
    app.use("/subcategories", subcategoryRouter)

    // creating 2 main categories
    const catRes = await request.post("/create")
      .send({ name: "Category" })
      .expect(200)

    const anotherCatRes = await request.post("/create")
      .send({ name: "Another Category" })
      .expect(200)

    // adding one subcategory for each category
    await request.post("/subcategories")
      .send({
        name: "SubCategory 1",
        categoryId: catRes.body.category._id,
      })
      .expect(200)

    await request.post("/subcategories")
      .send({
        name: "SubCategory 2",
        categoryId: anotherCatRes.body.category._id,
      })
      .expect(200)

    const res = await request.get("/graph")
      .expect(200)

    expect(res.body.total).toBe(2)
    expect(res.body.categories[0].categories.length).toBe(1)
    expect(res.body.categories[1].categories.length).toBe(1)
  })

  it("getting category options should return only the category options", async () => {
    app.use("/", categoryRouter)
    app.use("/subcategories", subcategoryRouter)

    // creating 2 main categories
    const catRes = await request.post("/create")
      .send({ name: "Category" })
      .expect(200)

    const anotherCatRes = await request.post("/create")
      .send({ name: "Another Category" })
      .expect(200)

    // adding one subcategory for each category
    await request.post("/subcategories")
      .send({
        name: "SubCategory 1",
        categoryId: catRes.body.category._id,
      })
      .expect(200)

    await request.post("/subcategories")
      .send({
        name: "SubCategory 2",
        categoryId: anotherCatRes.body.category._id,
      })
      .expect(200)

    const res = await request.get("/options")
      .expect(200)

    expect(res.body.options.length).toBe(2)
  })

  it("getting categories should not include subcategories", async () => {
    app.use("/", categoryRouter)
    app.use("/subcategories", subcategoryRouter)

    // creating 2 main categories
    const catRes = await request.post("/create")
      .send({ name: "Category" })
      .expect(200)

    const anotherCatRes = await request.post("/create")
      .send({ name: "Another Category" })
      .expect(200)

    // adding one subcategory for each category
    await request.post("/subcategories")
      .send({
        name: "SubCategory 1",
        categoryId: catRes.body.category._id,
      })
      .expect(200)

    await request.post("/subcategories")
      .send({
        name: "SubCategory 2",
        categoryId: anotherCatRes.body.category._id,
      })
      .expect(200)

    const res = await request.get("/")
      .expect(200)

    expect(res.body.categories.length).toBe(2)
  })

  it("removing a category with linked subcategories should not work", async () => {
    app.use("/", categoryRouter)
    app.use("/subcategories", subcategoryRouter)

    // creating 2 main categories
    const catRes = await request.post("/create")
      .send({ name: "Category" })
      .expect(200)

    // adding one subcategory for each category
    await request.post("/subcategories")
      .send({
        name: "SubCategory 1",
        categoryId: catRes.body.category._id,
      })
      .expect(200)

    const res = await request.delete("/")
      .send({
        name: "SubCategory 1",
        categoryId: catRes.body.category._id,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.SUBCATEGORIES_LINKED)
  })
})
