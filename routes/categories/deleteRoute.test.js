const mongoose = require("mongoose")
const { Category } = require("db/categories/collection")
const app = require("middleware/app")
const { responses } = require("db/categories/constants")
const supertest = require("supertest")

const categoryRouter = require("routes/categories/route")()
const subcategoryRouter = require("routes/sub-categories/route")()

const request = supertest(app)

const data = {
  name: "Shoes",
}

describe("categories delete route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Category.deleteMany({})
  })

  it("deleting a category should work", async () => {
    app.use("/", categoryRouter)

    let res = await request.post("/create")
      .send(data)
      .expect(200)

    expect(res.body.message).toBe(responses.CATEGORY_CREATED)

    const { category } = res.body
    res = await request.delete("/")
      .send({ categoryId: category._id })
      .expect(200)

    expect(res.body.message).toBe(responses.CATEGORY_DELETED)
  })

  it("deleting a category without providing a category id should not work", async () => {
    app.use("/", categoryRouter)

    let res = await request.post("/create")
      .send(data)
      .expect(200)

    expect(res.body.message).toBe(responses.CATEGORY_CREATED)
    res = await request.delete("/")
      .send({ })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_CATEGORY_ID)
  })

  it("deleting a category by providing an invalid category id should not work", async () => {
    app.use("/", categoryRouter)

    let res = await request.post("/create")
      .send(data)
      .expect(200)

    expect(res.body.message).toBe(responses.CATEGORY_CREATED)
    res = await request.delete("/")
      .send({ categoryId: "fake" })
      .expect(400)

    expect(res.body.message).toBe(responses.CATEGORY_ID_INVALID)
  })

  it("deleting a category by providing an inexistent category id should not work", async () => {
    app.use("/", categoryRouter)

    let res = await request.post("/create")
      .send(data)
      .expect(200)

    expect(res.body.message).toBe(responses.CATEGORY_CREATED)
    res = await request.delete("/")
      .send({ categoryId: "60819b161a8f936fabe2f899" })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_CATEGORY)
  })

  // TODO: a category should not be deleted if there are products with that category
})
