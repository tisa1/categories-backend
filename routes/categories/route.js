// const mongoose = require("mongoose")
const express = require("express")
const errorService = require("lib/errors/handler")

const { responses } = require("db/categories/constants")

const { Category } = require("db/categories/collection")
const routeSecurity = require("./security")
const middleware = require("./middleware")

const createRouter = () => {
  const categoryRouter = express.Router()

  // TODO: secure route creation
  categoryRouter.post("/create", [
    routeSecurity.create,
    middleware.optimizeFilters,
  ], async (req, res, next) => {
    try {
      const data = req?.body
      const category = await new Category(data).save()

      return res.json({ message: responses.CATEGORY_CREATED, category })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on categories /create route",
      })
    }
  })

  categoryRouter.get("/", [
  ], async (req, res, next) => {
    try {
      const projection = {
        name: 1,
      }

      const categories = await Category.find({ categoryId: null }, projection)
      const total = await Category.countDocuments()

      return res.json({ categories, total })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on categories / get route",
      })
    }
  })

  categoryRouter.get("/options", [
  ], async (req, res, next) => {
    try {
      const projection = {
        name: 1,
      }

      const options = await Category.find({ categoryId: null }, projection)

      return res.json({ options })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on categories /options route",
      })
    }
  })

  categoryRouter.get("/graph", [
  ], async (req, res, next) => {
    try {
      const projection = {
        name: 1,
        categories: 1,
      }

      const categories = await Category.aggregate([
        {
          $match: {
            categoryId: null,
          },
        },
        {
          $lookup: {
            from: "categories",
            let: { categoryId: "$_id" },
            pipeline: [
              { $match: { $expr: { $eq: ["$categoryId", "$$categoryId"] } } },
              {
                $project: {
                  name: 1,
                },
              },
            ],
            as: "categories",
          },
        },
        {
          $project: projection,
        },
      ])
      const total = await Category.countDocuments({ categoryId: null })

      return res.json({ categories, total })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on categories /graph route",
      })
    }
  })

  categoryRouter.delete("/", [
    routeSecurity.remove,
  ], async (req, res, next) => {
    try {
      const categoryId = req?.body?.categoryId
      await Category.findByIdAndRemove(categoryId)

      return res.json({ message: responses.CATEGORY_DELETED })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on categories / delete route",
      })
    }
  })

  return categoryRouter
}

module.exports = createRouter
